<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Owner extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mowner');
	}
	public function index()
	{
		$data['owner']=$this->Mowner->tampil_owner();
		$this->load->view('admin/header');
		$this->load->view('admin/owner', $data);
		$this->load->view('admin/footer');
	}

	public function hapus($id)
	{
		$this->Mowner->hapus($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">The data has been delete!</div>');
		redirect('admin/owner');
	}

	public function edit($id)
	{
		$post = $this->input->post();
		if ($post) {
			$this->Mowner->ambil_satu_data($post);
			$this->Mowner->update($post);
			redirect('admin/owner','refresh');
		}
		$this->load->view('admin/header');
		$this->load->view('admin/edit_owner');
		$this->load->view('admin/footer');
	}

}

/* End of file Owner.php */
/* Location: ./application/controllers/admin/Owner.php */